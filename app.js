const express = require("express");
const fetch = require("node-fetch");
const app = express();
app.use(express.json());
var geoip = require("geoip-lite");
const array = require("./ingredients");
const d = new Date();
const cors = require("cors");
let year = d.getFullYear();

app.use(cors("*"));

app.get("/cookTime", (req, res) => {
  let geo = geoip.lookup(req.ip);
  let ip = "";
  if (geo === null) {
    ip = "193.95.44.241";
    geo = geoip.lookup(ip);
  }

  const latitude = geo.ll[0];
  const longitude = geo.ll[1];
  fetch(
    `http://api.aladhan.com/v1/calendar?latitude=${latitude}&longitude=${longitude}&method=1&year=${year}&annual=true`
  )
    .then((response) => response.json())
    .then((data) => {
      //CorrectIngredient
      let CorrectIngredient = array.filter((dish) =>
        dish.ingredients.includes(req.query.ingredient)
      );

      if (CorrectIngredient.length === 0) {
        res.status(200).json({
          status: "fail",
          message: "invalid ingredient",
        });
        return;
      }
      const RamadanArr = [];
      //   ramadan dates
      for (i = 1; i < 13; i++) {
        for (j = 0; j < data.data[i].length; j++) {
          if (data.data[i][j].date.hijri.month.number === 9) {
            RamadanArr.push(data.data[i][j]);
          }
        }
      }
      //invalide date
      if (RamadanArr.length < req.query.date || req.query.date <= 0) {
        res.status(200).json({
          status: "fail",
          message: "invalid date",
        });
        return;
      }
      //far9 bin sa3at
      const sa3at =
        RamadanArr[req.query.date].timings.Maghrib.split(" ")[0].split(":")[0] -
        RamadanArr[req.query.date].timings.Asr.split(" ")[0].split(":")[0];
      //far9 bin d9ai9
      const d9ai9 =
        RamadanArr[req.query.date].timings.Maghrib.split(" ")[0].split(":")[1] -
        RamadanArr[req.query.date].timings.Asr.split(" ")[0].split(":")[1];

      //   console.log(sa3at, d9ai9);
      const elwa9etBinSletin = sa3at * 60 + d9ai9 - 15;

      let newDishes = [];
      for (i = 0; i < CorrectIngredient.length; i++) {
        CorrectIngredient[i].cooktime = "test";
        if (elwa9etBinSletin - CorrectIngredient[i].duration > 0) {
          CorrectIngredient[i].cooktime = ` ${
            elwa9etBinSletin - CorrectIngredient[i].duration
          } minutes after  Asr `;
        }

        if (elwa9etBinSletin - CorrectIngredient[i].duration < 0) {
          CorrectIngredient[i].cooktime = ` ${
            (elwa9etBinSletin - CorrectIngredient[i].duration) * -1
          } minutes  before Asr `;
        }
        newDishes.push(CorrectIngredient[i]);
      }

      res.status(200).json({
        status: "sucess",
        data: {
          newDishes,
        },
      });
    });
});

app.get("/suggest", (req, res) => {
  let geo = geoip.lookup(req.ip);
  let ip = "";
  if (geo === null) {
    ip = "193.95.44.241";
    geo = geoip.lookup(ip);
  }

  const latitude = geo.ll[0];
  const longitude = geo.ll[1];

  fetch(
    `http://api.aladhan.com/v1/calendar?latitude=${latitude}&longitude=${longitude}&method=1&year=${year}&annual=true`
  )
    .then((response) => response.json())
    .then((data) => {
      const RamadanArr = [];
      //   ramadan dates
      for (i = 1; i < 13; i++) {
        for (j = 0; j < data.data[i].length; j++) {
          if (data.data[i][j].date.hijri.month.number === 9) {
            RamadanArr.push(data.data[i][j]);
          }
        }
      }
      //invalide date
      if (RamadanArr.length < req.query.date || req.query.date <= 0) {
        res.status(200).json({
          status: "faile",
          message: "invalid date",
        });
        return;
      }
      //far9 bin sa3at
      const sa3at =
        RamadanArr[req.query.date].timings.Maghrib.split(" ")[0].split(":")[0] -
        RamadanArr[req.query.date].timings.Asr.split(" ")[0].split(":")[0];
      //far9 bin d9ai9
      const d9ai9 =
        RamadanArr[req.query.date].timings.Maghrib.split(" ")[0].split(":")[1] -
        RamadanArr[req.query.date].timings.Asr.split(" ")[0].split(":")[1];

      //   console.log(sa3at, d9ai9);
      const elwa9etBinSletin = sa3at * 60 + d9ai9 - 15;

      let newDishes = [];
      const a = Math.floor(Math.random() * array.length);
      console.log(a);
      array[a].cooktime = "";

      if (elwa9etBinSletin - array[a].duration > 0) {
        array[a].cooktime = ` ${
          elwa9etBinSletin - array[a].duration
        } minutes after  Asr `;
      }

      if (elwa9etBinSletin - array[a].duration < 0) {
        array[a].cooktime = ` ${
          (elwa9etBinSletin - array[a].duration) * -1
        } minutes  before Asr `;
      }
      newDishes.push(array[a]);

      res.status(200).json({
        status: "success",
        data: {
          newDishes,
        },
      });
    });
});

app.get("/ingredients", (req, res) => {
  let arr = [...new Set(array.map((x) => x.ingredients).flat())];
  res.status(200).json({
    status: "success",
    data: {
      arr,
    },
  });
});

app.get("/prayerTime", (req, res) => {
  let geo = geoip.lookup(req.ip);
  let ip = "";
  if (geo === null) {
    ip = "193.95.44.241";
    geo = geoip.lookup(ip);
  }

  const latitude = geo.ll[0];
  const longitude = geo.ll[1];
  fetch(
    `http://api.aladhan.com/v1/calendar?latitude=${latitude}&longitude=${longitude}&method=1&year=${year}&annual=true`
  )
    .then((response) => response.json())
    .then((d) => {
      const RamadanArr = [];
      //   ramadan dates
      for (i = 1; i < 13; i++) {
        for (j = 0; j < d.data[i].length; j++) {
          if (d.data[i][j].date.hijri.month.number === 9) {
            RamadanArr.push(d.data[i][j]);
          }
        }
      }
      res.status(200).json({
        status: "success",
        data: RamadanArr,
      });
    });
});

app.get("/getAllD", (req, res) => {
  res.status(200).json({
    status: "success",
    data: {
      array,
    },
  });
});


const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});
